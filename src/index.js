import React from 'react';
import ReactDOM from 'react-dom';
import FacebookLogin from 'react-facebook-login';
import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';
import graph from 'fbgraph';

const client = new ApolloClient({
  uri: 'http://localhost:4001/api'
});

const responseFacebook = (response) => {
  console.log(response);

  graph.setAccessToken(response.accessToken);

  var getFriends = function(url) {
    graph.get(url, function (err, res) {
      console.log(res);
      if (res.paging && res.paging.next) {
        getFriends(res.paging.next);
      }
    });
  };

  console.log(getFriends('/me/friends?summary=total_count'));


  return client.mutate({
    mutation: gql`
      mutation facebookAuth($email: String!, $uid: String!){
        facebookAuth(user: { email: $email, uid: $uid }) {
          email
          role
        }
      }
    `,
    variables: {
      "email": response.email,
      "uid": response.id
    }
  }).then((users) => console.log(users))
  .catch((error) => console.log(error))
}

ReactDOM.render(
  <FacebookLogin
    appId="1061183333915075"
    autoLoad={true}
    fields="name,email,picture"
    scope="user_friends"
    callback={responseFacebook}
    icon="fa-facebook"
  />,
  document.getElementById('root')
);

